#!/usr/bin/python
import rospy
import actionlib

# import these to create move base goals
from std_msgs.msg import Header
from geometry_msgs.msg import Pose
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

def move():
    header = Header(frame_id = 'map', stamp = rospy.Time.now())

    # create the Pose and MoveBaseGoal:



    # create the action client:



    # send the goal!



    # print on goal sent
    rospy.loginfo('GOAL SENT! o:')



    # print on finish
    rospy.loginfo('GOAL REACHED! (:')


if __name__ == '__main__':
    rospy.init_node('lasr_move_tutorial')
    try:
        move()
    except rospy.ROSInterruptException:
        pass