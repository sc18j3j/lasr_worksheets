#!/usr/bin/python
import rospy
from std_msgs.msg import String

def pub_for_no_reason():
    pub = rospy.Publisher('lasr_string_publisher', String, queue_size = 1)
    rospy.init_node('arbitrary_print')
    rate = rospy.Rate(1)

    while not rospy.is_shutdown():
        print 'print for no reason...'
        pub.publish(String('publish for no reason...'))
        rate.sleep()

if __name__ == '__main__':
    try:
        pub_for_no_reason()
    except rospy.ROSInterruptException:
        pass