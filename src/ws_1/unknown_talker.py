#!/usr/bin/python
import rospy
from std_msgs.msg import Int32

def pub_unknown():
    pub = rospy.Publisher('lasr_unknown_publisher', Int32, queue_size = 1)
    rospy.init_node('lasr_unknown_pub')
    rate = rospy.Rate(1)
    rospy.loginfo('unknown publisher up!')
    count = 0

    while not rospy.is_shutdown():
        pub.publish(Int32(count))
        count += 1
        rate.sleep()

if __name__ == '__main__':
    try:
        pub_unknown()
    except rospy.ROSInterruptException:
        pass